#!/usr/bin/env bash

echo "First arg: $1"
case $1 in
    flask)
        echo "Running uwsgi --socket 0.0.0.0:5000 --protocol http -w pigeon.web"
        exec uwsgi --socket 0.0.0.0:5000 --protocol http -w pigeon.web
        ;;
    celery)
        echo "Running celery worker --app=pigeon.worker --events --concurrency=4 --max-tasks-per-child=1"
        exec celery beat --app=pigeon.worker &
        exec celery worker --app=pigeon.worker --events --concurrency=4 --max-tasks-per-child=1
        ;;
esac