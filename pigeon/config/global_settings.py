import os
import dotenv
dotenv.load_dotenv()

DB = os.getenv('PIGEON_DB', "mongo://localhost:27017/pigeon")
BROKER = os.getenv("PIGEON_BROKER", "amqp://localhost:5672/pigeon")
BACKEND = os.getenv("PIGEON_BACKEND", "redis://localhost:6379/0")
BEAT = os.getenv("PIGEON_BEAT", "redis://localhost:6379/1")
