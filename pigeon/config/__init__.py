from ayarami import settings
from . import global_settings

settings.configure(global_settings)
__all__ = ["settings"]
