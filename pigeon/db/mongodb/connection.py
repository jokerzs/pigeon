from mongoengine import connect
from pigeon.config import settings

conn = connect(host=settings.DB)
