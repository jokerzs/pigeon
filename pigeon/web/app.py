import flask

from flask_swagger_ui import get_swaggerui_blueprint

from .blueprints.api import blueprint as api_blueprint

app = application = flask.Flask(__name__)

app.register_blueprint(api_blueprint, url_prefix="/api")
app.register_blueprint(get_swaggerui_blueprint(base_url='/docs', api_url='/static/swagger.json'), url_prefix="/docs")


@application.route('/debug/', methods=["GET", "POST"])
def debug():
    from flask import request
    if "fail" in request.args:
        0 / 0
    else:
        return "OK"
