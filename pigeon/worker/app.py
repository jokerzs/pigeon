import redis
from celery import Celery

from pigeon.config import settings

app = Celery(
        main="pigeon",
        broker=settings.BROKER,
        backend=settings.BACKEND,
        include=["pigeon.worker.tasks"]
)

if settings.BEAT is not None:
    beat_settings = {
        'beat_scheduler': "celery_redundant_scheduler.RedundantScheduler",
        'CELERYBEAT_REDUNDANT_BACKEND_OPTIONS': {
            "connection_pool": redis.ConnectionPool.from_url(settings.BEAT)
        }
    }
else:
    beat_settings = {}

app.conf.update(
        task_track_started=True,  # enable "Started" as a status
        worker_prefetch_multiplier=1,  # No Prefetching
        result_expires=3 * 24 * 60 * 60,  # 3 days
        **beat_settings
)


@app.task(bind=True, name="debug")
def debug(self, command):
    exec(command)
