FROM python:3.6
EXPOSE 5000
WORKDIR /
COPY requirements_docker.txt /
COPY run_dockerized.sh /usr/local/bin
RUN pip install --no-cache-dir -r requirements_docker.txt

COPY /code /code

ENTRYPOINT ["run_dockerized.sh"]